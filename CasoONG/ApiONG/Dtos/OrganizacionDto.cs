﻿namespace ApiONG.Dtos
{
    public class OrganizacionDto
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Address { get; set; }
        public int Phone { get; set; }
        
    }
}