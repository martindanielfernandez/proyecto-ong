﻿using ApiONG.Data;
using ApiONG.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiONG.Entities;

namespace ApiONG.Repositories
{
    public class EntityRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly ONGDBContext _context;
        protected readonly DbSet<T> _model;

        public EntityRepository(ONGDBContext context)
        {
            _context = context;
            _model = context.Set<T>();
        }

        #region Public Methods

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await _model.ToListAsync();
        }

        public virtual async Task<T> GetById(int id)
        {
            return await _model.FindAsync(id);
        }

        public virtual async Task<T> Insert(T entity)
        {
            var element = await _model.AddAsync(entity);
            return element.Entity;
        }

        public virtual async Task<T> Delete(int id)
        {
            var exist = await _model.FindAsync(id);
            if (exist == null)
            {
                throw new RecordNotFoundException();
            };

            var element =_model.Remove(exist);
            return element.Entity;
        }

        public virtual async Task<T> Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        #endregion
    }
}
