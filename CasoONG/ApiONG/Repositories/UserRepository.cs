﻿using ApiONG.Data;
using ApiONG.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Repositories
{
    public class UserRepository : EntityRepository<User>
    {
        public UserRepository(ONGDBContext context) : base(context)
        {
        }

        #region Public Methods

        public override async Task<IEnumerable<User>> GetAll()
        {
            var result = await _context.Users
                .OrderBy(x => x.Id)
                .ToListAsync();
            return result;
        }
        #endregion
    }
}
