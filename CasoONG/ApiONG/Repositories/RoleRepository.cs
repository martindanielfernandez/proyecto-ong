﻿using ApiONG.Data;
using ApiONG.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Repositories
{
    public class RoleRepository : EntityRepository<Role>
    {
        public RoleRepository(ONGDBContext context) : base(context)
        {
        }

        #region Public Methods

        public override async Task<IEnumerable<Role>> GetAll()
        {
            var result = await _context.Roles
                .OrderBy(x => x.Id)
                .ToListAsync();
            return result;
        }
        #endregion
    }
}
