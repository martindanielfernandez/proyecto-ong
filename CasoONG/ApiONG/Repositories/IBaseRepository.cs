﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiONG.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<T> Insert(T entity);
        Task<T> Delete(int id);
        Task<T> Update(T entity);
    }
}
