﻿using ApiONG.Data;
using ApiONG.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Repositories
{
    public class NewsRepository : EntityRepository<News>
    {
        public NewsRepository(ONGDBContext context ):base(context)
        {

        }
    }
}
