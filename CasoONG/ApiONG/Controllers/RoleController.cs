﻿using ApiONG.Commons;
using ApiONG.DTOs;
using ApiONG.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Controllers
{
    [Authorize]
    [Route("api/rols")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(RoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        [HttpGet("/getall")]
        public async Task<ActionResult<IEnumerable<RoleDTO>>> GetAll()
        {
            var result = await _roleService.GetAll();
            if (result.ToList().Count > 0)
            {
                return Ok(_mapper.Map<IEnumerable<RoleDTO>>(result));
            }
            return BadRequest(new { message = "Los Roles no fueron encontrados en el sistema" });
        }

        [HttpGet("/getbyid")]
        public async Task<ActionResult<IEnumerable<RoleDTO>>> GetRoleById([FromQuery] int id)
        {
            var result = await _roleService.GetById(id);
            if (result != null)
            {
                return Ok(_mapper.Map<RoleDTO>(result));
            }
            return BadRequest(new { message = $"El Role no fue encontrado en el sietema" });
        }

        [HttpPost("/insert")]
        public async Task<ActionResult<Result>> InsertRol([FromBody] RoleDTO request)
        {
            var insert = await _roleService.Insert(request);
            if (insert != null)
            {
                return new Result().Fail("Ya Existe un Registro de este Role");
            }
            else
            {
                var entity = _mapper.Map<RoleDTO>(request);

                await _roleService.Insert(entity);
                return new Result().Success($"Se ha agregado correctamente el Role {entity.Name} al sistema");
            }
        }

        [HttpDelete("/delete")]
        public async Task<ActionResult<Result>> DeleteRole([FromRoute] int id)
        {
            var delete = await _roleService.Delete(id);
            if (delete != null)
            {
                var entity = _mapper.Map<RoleDTO>(delete);

                await _roleService.Delete(entity.Id);
                return new Result().Success($"Se eliminó el Role {entity.Name} del sistema");
            }
            else
            {
                return new Result().NotFound();
            }
        }

        [HttpPut("/update")]
        public async Task<ActionResult<Result>> UpdateRole([FromBody] RoleDTO request)
        {
            var update = await _roleService.Update(request);
            if (update != null)
            {
                var entity = _mapper.Map<RoleDTO>(update);

                await _roleService.Update(entity);
                return new Result().Success($"Se han aplicado los cambios correctamente al Role {entity.Name} en el sistema");
            }
            else
            {
                return new Result().NotFound();
            }
        }
    }
}
