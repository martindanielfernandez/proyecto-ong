﻿using ApiONG.Entities;
using ApiONG.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Controllers
{
    [Route("api/activities")]
    public class ActivitiesController : Controller
    {
        private readonly ActivitiesService _service;
        public ActivitiesController(ActivitiesService service) 
        {
            _service = service;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _service.GetAll();
            return (result != null) ? Ok(result) : StatusCode(StatusCodes.Status500InternalServerError, new { message = "La solicitud no pudo ser procesada" });
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _service.GetById(id);
            return (result != null) ? Ok(result) : StatusCode(StatusCodes.Status500InternalServerError, new { message = "La solicitud no pudo ser procesada" });
        }
        [HttpPost]
        public async Task<IActionResult> Insert(Activity activity)
        {
            var result = await _service.Insert(activity);
            return (result != null) ? Ok(result) : StatusCode(StatusCodes.Status500InternalServerError, new { message = "La solicitud no pudo ser procesada" });
        }
        [HttpPut]
        public async Task<IActionResult> Update(Activity activity)
        {
            var result = await _service.Update(activity);
            return (result != null) ? Ok(result) : StatusCode(StatusCodes.Status500InternalServerError, new { message = "La solicitud no pudo ser procesada" });
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);
            return (result != null) ? Ok(result) : StatusCode(StatusCodes.Status500InternalServerError, new { message = "La solicitud no pudo ser procesada" });
        }
    }
}
