﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiONG.Dtos;
using ApiONG.Entities;
using ApiONG.Services;
using ApiONG.UnitOfWork;
using AutoMapper;

namespace ApiONG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizacionController : ControllerBase
    {
        private readonly OrganizacionService _organizacionService;
        private readonly IMapper _mapper;

        public OrganizacionController(OrganizacionService organizacionService, IMapper mapper)
        {
            _organizacionService = organizacionService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<Organizacion>>> GetAll()
        {
            var organizaciones =  await _organizacionService.GetAll();
            if (organizaciones.ToList().Count > 0)
            {
                return Ok(organizaciones);
            }

            return BadRequest(new { message = "Organizaciones no encontradas" });
        }

        
        [HttpGet("{id}")]
        public async Task<ActionResult<OrganizacionDto>> GetById(int id)
        {
            var organizacion = await _organizacionService.GetById(id);
            if (organizacion != null)
            {
                return _mapper.Map<Organizacion, OrganizacionDto>(organizacion);
            }
            return BadRequest(new { message = "Organizacion no encontrada" });
        }
        [HttpGet("public")]
        public async Task<ActionResult<OrganizacionDto>> GetOrganizacion()
        {
            var organizaciones = await _organizacionService.GetAll();
            if (organizaciones.ToList().Any())
            {
                return _mapper.Map<Organizacion, OrganizacionDto>(organizaciones.FirstOrDefault());
            }

            return BadRequest(new { message = "Organizacion no encontrada" });
        }
        [HttpPost]
        public async Task<IActionResult> Insert(Organizacion organizacion)
        {
            return Ok(await _organizacionService.Insert(organizacion));
        }

        [HttpPut]
        public async Task<IActionResult> Update(Organizacion organizacion)
        {
            return Ok(await _organizacionService.Update(organizacion));
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _organizacionService.Delete(id));
        }

    }
}
