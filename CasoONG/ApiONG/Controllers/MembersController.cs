﻿using ApiONG.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ApiONG.Entities;

namespace ApiONG.Controllers
{
    [ApiController]
    [Route("members")]
    public class MembersController : ControllerBase
    {
        private readonly MembersService _service;

        public MembersController(MembersService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _service.GetAll();
            return (result != null) ? Ok(result) : NoContent();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _service.GetById(id);
            return (result != null) ? Ok(result) : NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Insert(Member member)
        {
            var result = await _service.Insert(member);
            return (result != null) ? Ok() : BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Update(Member member)
        {
            var result = await _service.Update(member);
            return (result != null) ? Ok() : BadRequest();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);
            return (result != null) ? Ok() : BadRequest();
        }
    }
}
