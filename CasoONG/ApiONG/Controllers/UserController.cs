﻿using ApiONG.Commons;
using ApiONG.DTOs;
using ApiONG.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Controllers
{
    [Authorize]
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly IMapper _mapper;

        public UserController(UserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet("/getall")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetAllUsers()
        {
            var result = await _userService.GetAll();
            if (result.ToList().Count > 0)
            {
                return Ok(_mapper.Map<IEnumerable<UserDTO>>(result));
            }
            return BadRequest(new { message = "Los Users no fueron encontrados en el sistema" });
        }

        [HttpGet("/getbyid")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUserById([FromQuery] int id)
        {
            var result = await _userService.GetById(id);
            if (result != null)
            {
                return Ok(_mapper.Map<UserDTO>(result));
            }
            return BadRequest(new { message = $"El User no fue encontrado en el sietema" });
        }

        [HttpPost("/insert")]
        public async Task<ActionResult<Result>> InsertUser([FromBody] UserDTO request)
        {
            var insert = await _userService.Insert(request);
            if (insert != null)
            {
                return new Result().Fail("Ya Existe un Registro de este User");
            }
            else
            {
                var entity = _mapper.Map<UserDTO>(request);

                await _userService.Insert(entity);
                return new Result().Success($"Se ha agregado correctamente el User {entity.FirstName} al sistema");
            }
        }

        [HttpDelete("/delete")]
        public async Task<ActionResult<Result>> DeleteUser([FromRoute] int id)
        {
            var delete = await _userService.Delete(id);
            if (delete != null)
            {
                var entity = _mapper.Map<UserDTO>(delete);

                await _userService.Delete(entity.Id);
                return new Result().Success($"Se eliminó el User {entity.FirstName} del sistema");
            }
            else
            {
                return new Result().NotFound();
            }
        }

        [HttpPut("/update")]
        public async Task<ActionResult<Result>> UpdateUser([FromBody] UserDTO request)
        {
            var update = await _userService.Update(request);
            if (update != null)
            {
                var entity = _mapper.Map<UserDTO>(update);

                await _userService.Update(entity);
                return new Result().Success($"Se han aplicado los cambios correctamente al User {entity.FirstName} en el sistema");
            }
            else
            {
                return new Result().NotFound();
            }
        }
    }
}
