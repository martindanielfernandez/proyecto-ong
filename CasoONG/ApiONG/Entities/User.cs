﻿using ApiONG.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Entities
{
    [Index(nameof(Email), IsUnique = true)]
    public class User : ISoftDelete
    {

        /*Campos:id: INTEGER NOT NULL AUTO_INCREMENTfirstName: VARCHAR NOT NULLlastName: VARCHAR NOT NULLemail: VARCHAR UNIQUE NOT NULLpassword: VARCHAR NOT NULLphoto: VARCHAR NULLABLEroleId: Clave foranea hacia ID de Role timestamps y softDeletes*/

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        public string FirstName { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Column(TypeName = "varchar")]
        public string Email { get; set; }

      
        [Column(TypeName = "varchar")]
        public string Photo { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        public string Password { get; set; } 

        [ForeignKey("Role")]
        public int RoleId { get; set; }

        public Role Role { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime? ModificationDate { get; set; }
        public DateTime? DeletedDate { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
