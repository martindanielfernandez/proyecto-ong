﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApiONG.Entities
{
    public class Organizacion 
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Image { get; set; }
        public string Address { get; set; }
        public int Phone { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string WelcomeText { get; set; }
        public string AboutUs { get; set; }
        [DefaultValue(false)]
        public bool isDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }

        public Organizacion()
        {
            CreatedDate=DateTime.Now;
        }
    }

}