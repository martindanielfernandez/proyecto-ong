﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiONG.DTOs.Commands
{
    public class RegisterUser
    {
        [Required(ErrorMessage = "Nombre de usuario es necesario")]
        [StringLength(30, ErrorMessage = "Debe contener minimo 5 caracteres", MinimumLength = 5)]
        [Column(TypeName = "VARCHAR (30)")]
        public string Nombre { get; init; }
        [Required(ErrorMessage = "Nombre de usuario es necesario")]
        [StringLength(50, ErrorMessage = "Debe contener minimo 5 caracteres", MinimumLength = 5)]
        [Column(TypeName = "VARCHAR (50)")]
        public string Apellido { get; init; }
        [Required(ErrorMessage = "Correo electronico es necesario")]
        [StringLength(50, ErrorMessage = "Debe contener minimo 15 caracteres", MinimumLength = 15)]
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Debe ser un email valido")]
        [Column(TypeName = "VARCHAR (50)")]
        [EmailAddress]
        public string Email { get; init; }
        [Required(ErrorMessage = "Correo electronico es necesario")]
        [StringLength(100, ErrorMessage = "Debe contener minimo 10 caracteres", MinimumLength = 10)]
        [Column(TypeName = "VARCHAR (30)")]
        public string Contraseña { get; init; }
    }
}
