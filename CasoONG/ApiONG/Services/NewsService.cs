﻿using ApiONG.Entities;
using ApiONG.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class NewsService
    {
        private readonly UOW _unitOfWork;

        public NewsService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<News>> GetAll()
        {
            return await _unitOfWork.Repository<News>().GetAll();

        }

        public async Task<News> GetById(int id)
        {
            return await _unitOfWork.Repository<News>().GetById(id);
        }

        public async Task Insert(News News)
        {
            await _unitOfWork.Repository<News>().Insert(News);
            await _unitOfWork.Save();
        }

        public async Task Update(News News)
        {
            await _unitOfWork.Repository<News>().Update(News);
            await _unitOfWork.Save();
        }

        public async Task Delete(int id)
        {
            await _unitOfWork.Repository<News>().Delete(id);
            await _unitOfWork.Save();
        }
    }
}
