﻿using ApiONG.Entities;
using ApiONG.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class MembersService
    {
        private readonly UOW _unitOfWork;

        public MembersService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Member>> GetAll()
        {
            try
            {
                var result = await _unitOfWork.Repository<Member>().GetAll();
                await _unitOfWork.Save();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Member> GetById(int id)
        {
            try
            {
                var result = await _unitOfWork.Repository<Member>().GetById(id);
                await _unitOfWork.Save();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Member> Insert(Member entity)
        {
            try
            {
                var result = await _unitOfWork.Repository<Member>().Insert(entity);
                await _unitOfWork.Save();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Member> Update(Member entity)
        {
            try
            {
                var result = await _unitOfWork.Repository<Member>().Update(entity);
                await _unitOfWork.Save();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Member> Delete(int id)
        {
            try
            {
                var result = await _unitOfWork.Repository<Member>().Delete(id);
                await _unitOfWork.Save();
                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}
