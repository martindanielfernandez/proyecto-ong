﻿using ApiONG.Entities;
using ApiONG.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class CategoryService
    {
        private readonly UOW _unitOfWork;

        public CategoryService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _unitOfWork.Repository<Category>().GetAll();
        }

        public async Task<Category> GetById(int id)
        {
            return await _unitOfWork.Repository<Category>().GetById(id);
        }

        public async void Insert(Category category)
        {
            await _unitOfWork.Repository<Category>().Insert(category);
            await _unitOfWork.Save();
        }

        public async void Update(Category category)
        {
            await _unitOfWork.Repository<Category>().Update(category);
            await _unitOfWork.Save();
        }

        public async void Delete(int id)
        {
            await _unitOfWork.Repository<Category>().Delete(id);
            await _unitOfWork.Save();
        }
    }
}
