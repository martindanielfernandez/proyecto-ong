﻿using ApiONG.DTOs;
using ApiONG.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class UserService
    {
        private readonly UOW _unitOfWork;

        public UserService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return await _unitOfWork.Repository<UserDTO>().GetAll();
        }

        public async Task<UserDTO> GetById(int id)
        {
            return await _unitOfWork.Repository<UserDTO>().GetById(id);
        }

        public async Task<UserDTO> Insert(UserDTO entity)
        {
            var result = await _unitOfWork.Repository<UserDTO>().Insert(entity);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<UserDTO> Delete(int id)
        {
            var result = await _unitOfWork.Repository<UserDTO>().Delete(id);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<UserDTO> Update(UserDTO entity)
        {
            var result = await _unitOfWork.Repository<UserDTO>().Update(entity);
            await _unitOfWork.Save();
            return result;
        }
    }
}
