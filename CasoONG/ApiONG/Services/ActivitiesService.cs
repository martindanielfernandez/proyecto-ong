﻿using ApiONG.Entities;
using ApiONG.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class ActivitiesService
    {
        private readonly UOW _unitOfWork;
        
        public ActivitiesService(UOW unitOfWork) 
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Activity>> GetAll()
        {
            try
            {
                var result = await _unitOfWork.Repository<Activity>().GetAll();
                await _unitOfWork.Save();
                return result;
            }
            catch 
            {
                return null;
            }
        }

        public async Task<Activity> GetById(int id)
        {
            try
            {
                var result = await _unitOfWork.Repository<Activity>().GetById(id);
                await _unitOfWork.Save();
                return result;
            }
            catch 
            {
                return null;
            }
        }

        public async Task<Activity> Insert(Activity activity)
        {
           try
           {
                var result = await _unitOfWork.Repository<Activity>().Insert(activity);
                await _unitOfWork.Save();
                return result;
            }
           catch 
           { 
                return null; 
           }
        }

        public async Task<Activity> Update(Activity activity)
        {
            try
            {
                var result = await _unitOfWork.Repository<Activity>().Update(activity);
                await _unitOfWork.Save();
                return result;
            }
            catch 
            { 
                return null; 
            }
        }

        public async Task<Activity> Delete(int id)
        {
            try
            {
                var result = await _unitOfWork.Repository<Activity>().Delete(id);
                await _unitOfWork.Save();
                return result;
            }
            catch 
            { 
                return null; 
            }
        }
    }
}
