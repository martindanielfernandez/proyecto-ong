﻿using ApiONG.Entities;
using ApiONG.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class OrganizacionService
    {
        private readonly UOW _unitOfWork;

        public OrganizacionService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Organizacion>> GetAll()
        {
            var result = await _unitOfWork.Repository<Organizacion>().GetAll();
            await _unitOfWork.Save();
            return result;
        }

        public async Task<Organizacion> GetById(int id)
        {
            var result = await _unitOfWork.Repository<Organizacion>().GetById(id);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<Organizacion> Insert(Organizacion organizacion)
        {
            var result = await _unitOfWork.Repository<Organizacion>().Insert(organizacion);
            await  _unitOfWork.Save();
            return result;
        }

        public async Task<Organizacion> Update(Organizacion organizacion)
        {
            var result = await _unitOfWork.Repository<Organizacion>().Update(organizacion);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<Organizacion> Delete(int id)
        {
            var result = await _unitOfWork.Repository<Organizacion>().Delete(id);
            await _unitOfWork.Save();
            return result;
        }
    }
}