﻿using ApiONG.DTOs;
using ApiONG.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiONG.Services
{
    public class RoleService
    {
        private readonly UOW _unitOfWork;

        public RoleService(UOW unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RoleDTO>> GetAll()
        {
            return await _unitOfWork.Repository<RoleDTO>().GetAll();
        }

        public async Task<RoleDTO> GetById(int id)
        {
            return await _unitOfWork.Repository<RoleDTO>().GetById(id);
        }

        public async Task<RoleDTO> Insert(RoleDTO entity)
        {
            var result = await _unitOfWork.Repository<RoleDTO>().Insert(entity);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<RoleDTO> Delete(int id)
        {
            var result = await _unitOfWork.Repository<RoleDTO>().Delete(id);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<RoleDTO> Update(RoleDTO entity)
        {
            var result = await _unitOfWork.Repository<RoleDTO>().Update(entity);
            await _unitOfWork.Save();
            return result;
        }
    }
}
