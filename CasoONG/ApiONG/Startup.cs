using ApiONG.Data;
using ApiONG.Services.EmailProvider;
using ApiONG.Profiles;
using ApiONG.Repositories;
using ApiONG.Services;
using ApiONG.UnitOfWork;
using ApiONG.Helpers;
using ApiONG.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ApiONG
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /// Add DbContext
            services.AddDbContext<ONGDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ONGDBConnection")));

            /// Add Automapper
            services.AddAutoMapper(typeof(RoleProfile));
            services.AddAutoMapper(typeof(UserProfile));

            /// Add Repositories
            services.AddScoped<RoleRepository>();
            services.AddScoped<UserRepository>();

            /// Add Services
            services.AddScoped<RoleService>();
            services.AddScoped<UserService>();
            services.AddScoped<OrganizacionService>();
            services.AddScoped<NewsService>();
            services.AddScoped<MembersService>();
            services.AddScoped<ActivitiesService>();

            services.AddScoped<UOW>();

            services.AddControllers();

            services.AddAutoMapper(typeof(MappingProfile));

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "ApiONG", Version = "v1" }); });

            //Agrega Servicio para env�o de email con SendGrid
            services.AddTransient<IEmailService, SendGridEmailService>();
            services.Configure<SendGridEmailServiceOptions>(options =>
            {
                options.ApiKey = Configuration["ExternalProviders:SendGrid:ApiKey"];
                options.SenderEmail = Configuration["ExternalProviders:SendGrid:SenderEmail"];
                options.SenderName = Configuration["ExternalProviders:SendGrid:SenderName"];
            });            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ApiONG v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
