﻿using ApiONG.DTOs.Commands;
using ApiONG.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApiONG.Data
{
    public class ONGDBContext : DbContext
    {

        public ONGDBContext(DbContextOptions<ONGDBContext> options) : base(options)
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Organizacion> Organizaciones{ get; set; }
        public DbSet<RegisterUser> RegisterUsers { get; set; }
    }
}
