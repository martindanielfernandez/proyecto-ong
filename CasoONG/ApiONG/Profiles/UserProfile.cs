﻿using ApiONG.DTOs;
using ApiONG.DTOs.Commands;
using ApiONG.Entities;
using AutoMapper;

namespace ApiONG.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<User, RegisterUser>().ReverseMap();
        }
    }
}
