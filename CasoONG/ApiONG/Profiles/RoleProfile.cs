﻿using ApiONG.DTOs;
using ApiONG.Entities;
using AutoMapper;

namespace ApiONG.Profiles
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleDTO>().ReverseMap();
        }
    }
}
