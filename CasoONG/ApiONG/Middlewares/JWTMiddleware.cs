﻿using ApiONG.Middlewares;
using ApiONG.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiONG.Helpers;
using ApiONG.Entities;

namespace ApiONG.Middlewares
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _config;
        private readonly UOW _unitOfWork;

        public JwtMiddleware(RequestDelegate next, IConfiguration config, UOW unitOfWork)
        {
            _next = next;
            _config = config;
            _unitOfWork = unitOfWork;
        }

        public async Task Invoke(HttpContext context)
        {

            var userId = new JWTHelper(_config).ValidateToken(context);
            var user = (userId != null) ? _unitOfWork.Repository<User>().GetById(Convert.ToInt32(userId)) : null;
            context.Items["User"] = user;
            await _next(context);
        }

        
    }
}