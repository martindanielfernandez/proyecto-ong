﻿using ApiONG.Dtos;
using ApiONG.Entities;
using AutoMapper;

namespace ApiONG.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Organizacion, OrganizacionDto>();
        }

    }
}