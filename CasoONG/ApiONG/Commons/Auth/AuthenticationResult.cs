﻿using System.Collections.Generic;

namespace ApiONG.Commons.Auth
{
    public class AuthenticationResult : Result
    {
        public string Token { get; init; }
        public IEnumerable<string> ErrorMessages { get; init; }
    }
}
