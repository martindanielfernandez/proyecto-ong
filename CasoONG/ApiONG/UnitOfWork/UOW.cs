﻿using System;
using System.Collections;
using System.Threading.Tasks;
using ApiONG.Data;
using ApiONG.Repositories;

namespace ApiONG.UnitOfWork
{
    public class UOW
    {
        private ONGDBContext _context;

        #region Repositories Declaration

        /* Example */
        //private EntityRepository _entityRepository;
        private Hashtable _repositories;

        #endregion

        public UOW(ONGDBContext context)
        {
            _context = context;
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #region Repositories Implementation

        /* Example */
        public IBaseRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (_repositories == null) _repositories = new Hashtable();

            var type = typeof(TEntity).Name;

            if (!_repositories.ContainsKey(type))
            {
                var repositoryType = typeof(EntityRepository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context);

                _repositories.Add(type, repositoryInstance);
            }

            return (IBaseRepository<TEntity>)_repositories[type];
        }

        #endregion        
    }
}
