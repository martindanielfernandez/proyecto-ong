﻿using ApiONG.Commons;
using ApiONG.Commons.Auth;
using ApiONG.Data;
using ApiONG.DTOs.Commands;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CasoONG.Controllers
{
    [Authorize]
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private ONGDBContext _context;
        private readonly IMapper _mapper;

        public AuthController(ONGDBContext context,
                            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost("/register")]
        public async Task<ActionResult<AuthenticationResult>> UserRegister([FromBody] RegisterUser request)
        {
            var existUser = await _context.FindAsync<RegisterUser>(request.Email);
            if (existUser != null)
                return ValidateUserException(AuthValidationErrorResponses.UserAlreadyExist);

            var entity = _mapper.Map<RegisterUser>(request);
            var Encrypt = Encriptar.GetSHA256(entity.Contraseña);

            var createUser = await _context.AddAsync(entity);

            if (createUser == null)
            {
                {
                    return (ActionResult<AuthenticationResult>)new Result().Fail("Ya Existe un Registro de este User");
                };
            }
            await _context.AddAsync(Encrypt);
            return (ActionResult<AuthenticationResult>)new Result().Success($"Se ha Resgitrado correctamente el User {entity.Email} al sistema");
        }
        private AuthenticationResult ValidateUserException(string validationMessage)
        {
            return new AuthenticationResult
            {
                ErrorMessages = new[] { validationMessage }
            };
        }
    }
}
